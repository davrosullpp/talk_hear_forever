exports.definition = {
  config: {
    columns: {
      "id":"int PRIMARY KEY",
      "title":"text",
      "content":"text",
      "audio":"text",
      "image":"text",
    },
    adapter: {
      type: "sql",
      collection_name: "stories",
      migration : "201412060350928",
      idAttribute: 'id'
    },
  },
  extendModel: function(Model) {
    _.extend(Model.prototype, {});
    return Model;
  },
  extendCollection: function(Collection) {
    _.extend(Collection.prototype, {

      comparator: function (collection) {
        return collection.get("title");
      }
    });
    return Collection;
  }
}
