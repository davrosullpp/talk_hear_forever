exports.definition = {
  config: {
    columns: {
      "id": "text PRIMARY KEY",
      "caption":"INTEGER",
      "tags":"text",
      "image":"text",
      "profile":"text",
      "username":"text",
    },
    adapter: {
      type: 'json',
      url: "https://api.instagram.com/v1/tags/MorningtonIsland/media/recent?access_token=10385355.1fb234f.65b1411b36424010820ebdba0f312250"
    },
  },
  extendModel: function(Model) {
    _.extend(Model.prototype, {});
    return Model;
  },
  extendCollection: function(Collection) {
    _.extend(Collection.prototype, {
      parse : function(_resp, xhr) {
        this.next_page = _resp.next_page;

        var entries = [];
        _.each(_resp.data, function(_entry) {
          console.log("---davro---");
          // console.log(_entry);
          var entry = {};
          entry.caption =  (_entry.caption != null) ? _entry.caption.text : "";
          entry.username = _entry.user.username;
          entry.profile = _entry.user.profile_picture;
          entry.tags = _entry.tags;
          entry.image = _entry.images.standard_resolution.url;
          entry.cityId = Alloy.Globals.cityId;
          // console.log(entry);
          entries.push(entry);
        });

        return entries;
      },

      comparator: function (collection) {
        return collection.get("title");
      }
    });
    return Collection;
  }
}
