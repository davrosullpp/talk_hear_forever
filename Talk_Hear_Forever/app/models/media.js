exports.definition = {
  config: {
    columns: {
      "id":"int PRIMARY KEY",
      "type":"text",
      "title":"text",
      "content":"text",
      "audio":"text",
      "image":"text",
      "url":"text",
    },
    adapter: {
      type: "sql",
      collection_name: "media",
      migration : "20141206043516",
      idAttribute: 'id'
    },
  },
  extendModel: function(Model) {
    _.extend(Model.prototype, {});
    return Model;
  },
  extendCollection: function(Collection) {
    _.extend(Collection.prototype, {

      comparator: function (collection) {
        return collection.get("title");
      }
    });
    return Collection;
  }
}
