exports.definition = {
  config: {
    columns: {
      "id":"integer PRIMARY KEY",
      "type":"text",
      "language":"text",
      "word":"text",
      "english":"text",
      "image":"text",
      "audio":"text",
    },
    adapter: {
      type: "sql",
      collection_name: "language",
      migration : "201412061146540",
      idAttribute: 'id'
    },
  },
  extendModel: function(Model) {
    _.extend(Model.prototype, {});
    return Model;
  },
  extendCollection: function(Collection) {
    _.extend(Collection.prototype, {

      comparator: function (collection) {
        return collection.get("word");
      }
    });
    return Collection;
  }
}
