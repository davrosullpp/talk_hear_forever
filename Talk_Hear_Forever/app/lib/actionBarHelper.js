exports.setupActionBar = function(window, custom, displayHomeAsUp) {
  if (!OS_ANDROID) return;

  var abx = require('com.alcoapps.actionbarextras');
  window.addEventListener("open", function() {
    abx.titleFont = "MI.otf";
    abx.backgroundColor = "#000";
    abx.titleColor      = "#fff";
    abx.title = window.title;
    if (custom) custom(abx);

    var actionBar = window.activity.actionBar;
    if (actionBar) {
      actionBar.displayHomeAsUp = displayHomeAsUp != undefined ? displayHomeAsUp : true;
      actionBar.onHomeIconItemSelected = function() {
        window.close();
      };
    }
  });
}
