var languageSeed = [{"id":1,"language":"creole","type":"phrase","word":"","english":"","image":"image1.png","audio":"phrase1.mp3"},
{"id":2,"language":"creole","type":"phrase","word":"","english":"","image":"image2.png","audio":"phrase2.mp3"},
{"id":3,"language":"creole","type":"phrase","word":"","english":"","image":"image3.png","audio":"phrase3.mp3"},
{"id":4,"language":"creole","type":"phrase","word":"","english":"","image":"image4.png","audio":"phrase4.mp3"},
{"id":5,"language":"creole","type":"phrase","word":"","english":"","image":"image5.png","audio":"phrase5.mp3"},
{"id":6,"language":"language","type":"word","word":"kendabol","english":"dugong","image":"","audio":"word1.mp3"},
{"id":7,"language":"language","type":"word","word":"mulgry","english":"sick","image":"","audio":"word2.mp3"},
{"id":8,"language":"language","type":"word","word":"thaldi","english":"come","image":"","audio":"word3.mp3"},
{"id":9,"language":"language","type":"word","word":"bana","english":"and","image":"","audio":"word4.mp3"},
{"id":10,"language":"language","type":"word","word":"ditha","english":"sit","image":"","audio":"word5.mp3"},
{"id":11,"language":"language","type":"word","word":"ninghi","english":"you","image":"","audio":"word6.mp3"},
{"id":12,"language":"language","type":"word","word":"thabu","english":"big brother","image":"","audio":"word7.mp3"},
{"id":13,"language":"language","type":"word","word":"mangalda","english":"baby","image":"","audio":"word8.mp3"},
{"id":14,"language":"language","type":"word","word":"booya","english":"prawn","image":"","audio":"word9.mp3"},
{"id":15,"language":"language","type":"word","word":"thuldid","english":"pregnant","image":"","audio":"word10.mp3"},
{"id":16,"language":"language","type":"word","word":"banji","english":"brother/sister inlaw","image":"","audio":"word11.mp3"},
{"id":17,"language":"language","type":"word","word":"ngama","english":"mother","image":"","audio":"word12.mp3"},
{"id":18,"language":"language","type":"word","word":"yarraman","english":"horse","image":"","audio":"word13.mp3"},
{"id":19,"language":"language","type":"word","word":"thuwathu","english":"rainbow serpant","image":"","audio":"word14.mp3"},
{"id":20,"language":"language","type":"word","word":"kunu","english":"small sister/brother","image":"","audio":"word15.mp3"},
{"id":21,"language":"language","type":"word","word":"kunatha","english":"goodbye","image":"","audio":"word16.mp3"}];

migration.up = function(db) {
  db.createTable({
    "columns" : {
      "id":"integer PRIMARY KEY",
      "type":"text",
      "language":"text",
      "word":"text",
      "english":"text",
      "image":"text",
      "audio":"text",
    }
  });
  for (var i = 0; i < languageSeed.length; i++) {
    console.log(languageSeed[i].title);
    db.insertRow({
      id : languageSeed[i].id,
      type : languageSeed[i].type,
      language : languageSeed[i].language,
      word : languageSeed[i].word,
      english : languageSeed[i].english,
      audio : languageSeed[i].audio,
      image : languageSeed[i].image
    });
  }
};

migration.down = function(db) {
  db.dropTable();
};
