var mediaSeed = [{"id":1,"type":"story","title":"Jade’s Story","content":null,"audio":"jade.mp3","image":null,"url":null},
{"id":2,"type":"story","title":"Steve’s Story","content":null,"audio":"jade.mp3","image":null,"url":null},
{"id":3,"type":"story","title":"Greg’s Story","content":null,"audio":"jade.mp3","image":null,"url":null},
{"id":4,"type":"story","title":"Patricia’s Story","content":null,"audio":"jade.mp3","image":null,"url":null},
{"id":5,"type":"story","title":"Mornington Island’s Story","content":null,"audio":"jade.mp3","image":null,"url":null},
{"id":6,"type":"story","title":"Tom’s Story","content":null,"audio":"jade.mp3","image":null,"url":null},
{"id":7,"type":"song","title":"Hello Song","content":null,"audio":"doggiedance.mp3","image":null,"url":null},
{"id":8,"type":"song","title":"Goodbye Song","content":null,"audio":"doggiedance.mp3","image":null,"url":null},
{"id":9,"type":"song","title":"Happy Song","content":null,"audio":"doggiedance.mp3","image":null,"url":null},
{"id":10,"type":"song","title":"Goodbye Song","content":null,"audio":"doggiedance.mp3","image":null,"url":null},
{"id":11,"type":"song","title":"Goodbye Song","content":null,"audio":"doggiedance.mp3","image":null,"url":null}];


migration.up = function(db) {
  db.createTable({
    "columns" : {
      "id":"int PRIMARY KEY",
      "type":"text",
      "title":"text",
      "content":"text",
      "audio":"text",
      "image":"text",
      "url":"text"
    }
  });
  for (var i = 0; i < mediaSeed.length; i++) {
    console.log(mediaSeed[i].title);
    db.insertRow({
      id : mediaSeed[i].id,
      type : mediaSeed[i].type,
      title : mediaSeed[i].title,
      content : mediaSeed[i].content,
      audio : mediaSeed[i].audio,
      image : mediaSeed[i].image,
      url : mediaSeed[i].url
    });
  }
};

migration.down = function(db) {
  db.dropTable();
};
