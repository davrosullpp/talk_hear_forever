var storiesSeed = [
  {"id":"1", "title":"Boy cried wolf", "content":"there once was a boy", "audio":"", "image":""}
];

migration.up = function(db) {
  db.createTable({
    "columns" : {
      "id":"int PRIMARY KEY",
      "title":"text",
      "content":"text",
      "audio":"text",
      "image":"text",
    }
  });
  for (var i = 0; i < storiesSeed.length; i++) {
    console.log(storiesSeed[i].title);
    db.insertRow({
      id : storiesSeed[i].id,
      title : storiesSeed[i].title,
      content : storiesSeed[i].content,
      audio : storiesSeed[i].audio,
      image : storiesSeed[i].image
    });
  }
};

migration.down = function(db) {
  db.dropTable();
};
