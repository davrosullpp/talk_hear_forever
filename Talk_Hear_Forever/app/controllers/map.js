var args = arguments[0] || {};

function closewin(){
  $.getView().close();
}


$.mapview.setLocation({
  animate : true,
  latitude:"-16.5",
  longitude:"139.5",
  latitudeDelta:0.9,
  longitudeDelta:0.9,
});
