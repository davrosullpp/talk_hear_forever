var args = arguments[0] || {};

var ui = require('ui');
var imageView = ui.createView({
  height : (OS_IOS) ? Alloy.CFG.headerHeight+50 : Alloy.CFG.headerHeight+100,
  width : Ti.UI.FILL,
  top:0,
  backgroundImage: "http://upload.wikimedia.org/wikipedia/commons/0/05/Mornington_island.jpg",
  backgroundSize: 'cover'
});

setTimeout(function(){
  var dialog = Ti.UI.createAlertDialog({
    message: 'This app may contain pictures and voices of Aboriginal people who have passed away',
    ok: 'Okay',
    title: 'Be careful!'
  });
  dialog.show();
},1000);


$.headerImage.add(imageView);

if(OS_IOS) {
  var TandemScroll = require('ti.tandemscroll');
  var scrollViews =[$.scrollImage, $.scrollView];
  TandemScroll.lockTogether(scrollViews);
}

function openContent(e){
  if(OS_IOS) $.list.deselectItem(e.sectionIndex, e.itemIndex);

  var item = $.list.sections[e.sectionIndex].getItemAt(e.itemIndex);
  switch(item.title.text){
    case "Our Stories":
      Alloy.Globals.open('stories');
      break;
    case "Our Language":
      Alloy.Globals.open('language');
      break;
    case "Our Songs":
      Alloy.Globals.open('songs');
      break;
    case "Images":
      Alloy.Globals.open('instagram');
      break;
    case "About Mornington Island":
      Alloy.Globals.open('about');
      break;
    }

  }
