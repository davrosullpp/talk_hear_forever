var args = arguments[0] || {},
    soundTimer;

require('actionBarHelper').setupActionBar($.getView());

function closewin(){
  $.getView().close();
}

function playAudio(){

}

$.player.title = args.title;

function fakeProgress(){
  var val = 0;
  var pInterval = setInterval(function(){
    $.progressBar.value = ++val;
    if (val > 100) clearInterval(pInterval);
  }, 30);
}

// fakeProgress()
$.progressBar.show();

var audioFile = Ti.Media.createSound({url:"/audio/doggiedance.mp3"});


function playAudio() {

  if (audioFile.playing) {
    audioFile.pause();
    clearInterval(soundTimer);
  } else {
    audioFile.play();
    soundTimer = setInterval(function() {
      if (OS_ANDROID && $.audioLength.text == "0:00") {
        $.length.text = secondsToTime((audioFile.duration / 1000));
        audioLength = audioFile.duration;
      }

      var percent = audioFile.time / audioFile.duration * 100;
      if (OS_IOS) percent = percent / 1000;

    if (percent > $.progressBar.getValue()) $.progressBar.value = (percent);
    }, 100);
  }
}



audioFile.addEventListener('complete', function () {
  audioComplete = true;
  clearInterval(soundTimer);
  audioFile.pause();
});


function secondsToTime(secs) {
  console.log("seconds:" +secs);
  var hours = Math.floor(secs / (60 * 60));

  var divisor_for_minutes = secs % (60 * 60);
  var minutes = Math.floor(divisor_for_minutes / 60);

  var divisor_for_seconds = divisor_for_minutes % 60;
  var seconds = Math.ceil(divisor_for_seconds);
  seconds = (seconds < 10) ? "0" + seconds : seconds;

  var obj = {
    "h": hours,
    "m": minutes,
    "s": seconds
  };
  return minutes + ":" + seconds;
}
