/*
Even though your Views are platform-specific, you can still keep your controllers cross-platform.
This controller will be used for your home View regardless of the platform, so you'll have to
manually check in case you need to perform platform-specific operations
*/

var args = arguments[0] || {};


require('actionBarHelper').setupActionBar($.getView());

function doopen(evt){

}

function doadd(evt){
	if (OS_IOS){
		$.nav.openWindow(Alloy.createController('secondwin').getView());
	}else if (OS_ANDROID){
		Alloy.createController('secondwin').getView().open();
	}
}

if(OS_IOS){
	Alloy.Globals.NAV = $.nav;
}

Alloy.Globals.open = function(controller, args){
	if (OS_IOS){
		$.nav.openWindow(Alloy.createController(controller, args).getView());
	}else if (OS_ANDROID){
		Alloy.createController(controller, args).getView().open();
	}
};
