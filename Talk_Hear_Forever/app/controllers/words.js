var args = arguments[0] || {};

require('actionBarHelper').setupActionBar($.getView());

function closewin(){
  $.destroy();
  $.getView().close();
}

function filterFunction(collection) {
  return collection.where({'type':'word'});
}

function playAudio(e){
  if(OS_IOS) $.list.deselectItem(e.sectionIndex, e.itemIndex);
  var item = $.list.sections[e.sectionIndex].getItemAt(e.itemIndex);
  console.log(item.audio.audioFile);
  var player = Ti.Media.createSound({url:"/audio/"+item.audio.audioFile});
  player.play();
}

Alloy.Collections.language.fetch();
