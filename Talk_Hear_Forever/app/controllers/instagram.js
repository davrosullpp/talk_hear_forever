var args = arguments[0] || {};

// var instagram = Alloy.Collections.instagram;
//
// instagram.fetch();

// $.listCollection.fetch();


require('actionBarHelper').setupActionBar($.getView());


var initialised = false;
function focus() {
  if(initialised) {
    $.container.updateListIfErrorOrStale();
  } else {
    initialised = true;

    $.container.bindList({
      pullToRefresh:    true,
      infiniteScroll:   false,
      refreshLabel:     "Instagram Images",
      list:             $.listCollection,
      listView:         $.lists,
      staleSeconds:     10 * 60
    });
  }
};

// This could be called to clean up memory if we closed this window
exports.cleanup = function(){
  $.destroy();
  $.off();
  $.container.cleanup();
};

function closewin(){
  exports.cleanup();
  $.getView().close();
}




function itemClick(e){
  var section = $.lists.sections[e.sectionIndex];
  var item = section.getItemAt(e.itemIndex);
  console.log("clicked on item at itemId: " + e.itemIndex + " - " + JSON.stringify(e) + ")");
  Ti.Platform.openURL("http://instagram.com/" + item.user.text);
}
