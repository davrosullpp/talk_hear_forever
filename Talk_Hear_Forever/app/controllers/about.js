var args = arguments[0] || {};

require('actionBarHelper').setupActionBar($.getView());

var ui = require('ui');
var imageView = ui.createView({
  height : (OS_IOS) ? Alloy.CFG.headerHeight+50 : Alloy.CFG.headerHeight+100,
  width : Ti.UI.FILL,
  top:30,
  backgroundImage: "images/photos/image6.png",
  backgroundSize: 'cover'
});
$.headerImage.add(imageView);

if(OS_IOS) {
  var TandemScroll = require('ti.tandemscroll');
  var scrollViews =[$.scrollImage, $.scrollView];
  TandemScroll.lockTogether(scrollViews);
}

function closewin(){
  $.destroy();
  $.getView().close();
}

function openContent(e){
  if(OS_IOS) $.list.deselectItem(e.sectionIndex, e.itemIndex);

  var item = $.list.sections[e.sectionIndex].getItemAt(e.itemIndex);
  var item = $.list.sections[e.sectionIndex].getItemAt(e.itemIndex);
  switch(item.title.text){
    case "Mornington Island":
      Alloy.Globals.open('mornington');
      break;

      case "Map":
        Alloy.Globals.open('map');
        break;

      }
}
