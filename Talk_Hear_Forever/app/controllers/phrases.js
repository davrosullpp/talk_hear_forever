var args = arguments[0] || {};

require('actionBarHelper').setupActionBar($.getView());

function closewin(){
  $.destroy();
  $.getView().close();
}

function transformData(model) {
  var transform = model.toJSON();
  transform.imageFile = "/images/phrases/" + transform.image;
  console.log(transform);
  return transform;
}

function filterFunction(collection) {
  return collection.where({'type':'phrase'});
}


function playAudio(e){
  var player = Ti.Media.createSound({url:"/audio/"+e.source.audioFile});
  player.play();
}

Alloy.Collections.language.fetch();
