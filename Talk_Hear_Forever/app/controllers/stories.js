var args = arguments[0] || {};

require('actionBarHelper').setupActionBar($.getView());

var audioFile = Ti.Media.createSound({url:"/audio/test1.mp3"});


var ui = require('ui');
var imageView = ui.createView({
  height : (OS_IOS) ? Alloy.CFG.headerHeight+50 : Alloy.CFG.headerHeight+100,
  width : Ti.UI.FILL,
  top:0,
  backgroundImage: "images/photos/image5.jpeg",
  backgroundSize: 'cover'
});
$.headerImage.add(imageView);

if(OS_IOS) {
  var TandemScroll = require('ti.tandemscroll');
  var scrollViews =[$.scrollImage, $.scrollView];
  TandemScroll.lockTogether(scrollViews);
}

function closewin(){
  $.destroy();
  if (audioFile.playing) {
    audioFile.pause();
    audioFile.stop();
  }
  $.getView().close();
}

function openContent(e){
  if(OS_IOS) $.list.deselectItem(e.sectionIndex, e.itemIndex);
  var item = $.list.sections[e.sectionIndex].getItemAt(e.itemIndex);
  if (audioFile.playing) {
    audioFile.stop();
  }
  audioFile = Ti.Media.createSound({url:"/audio/"+item.title.audioFile});
  playAudio();

}

function filterFunction(collection) {
  return collection.where({'type':'story'});
}


function playAudio(audio) {
  if (audioFile.playing) {
    audioFile.pause();
  } else {
    audioFile.play();
  }
}

Alloy.Collections.media.fetch();
